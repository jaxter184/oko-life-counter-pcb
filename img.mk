NAME := oko

svg: $(addprefix img/target/, FMask.layer.png FCu.layer.png FSilk.layer.png)

img/target/%.layer.png: img/$(NAME).svg
	mkdir img/target -p
	inkscape $^ -jC -i $* -e $@ --export-dpi=2400

svg-clean:
	rm img/target/*

footprint_dir = pcb/card-art

collate: $(footprint_dir)/$(NAME).kicad_mod

$(footprint_dir)/$(NAME).kicad_mod: $(addprefix $(footprint_dir)/, $(NAME)fsilk.kicad_mod $(NAME)fcu.kicad_mod $(NAME)fmask.kicad_mod)
	cd $(footprint_dir) && ./collate.py

.PHONY: svg-clean svg collate

## Overview

This project is a life counter for Magic the Gathering and other games where a player is in charge of keeping track of their life total. 

## To make a new card:

* Edit the `NAME` variable in `img.mk` to fit your new card
* Copy the existing svg file, edit that new file to fit the card you want. Pay attention to the layers that each shape is on.
* Run `make svg` to convert the svg file to its constituent PCB layers.
* Use KiCAD's bitmap2component tool to convert each image to the PCB CAD graphic layers (use the `$(NAME).Layer.kicad_mod` naming scheme)
* Run `make collate` to combine the separate layers into one and get rid of the registration rectangles
* Add the combined footprint to the PCB (and remove the old one)

## Software/Hardware Compatibility

Hardware version is written on the board, software version is in the repository

| Hardware | Software |
| -------- | -------- |
| 0.1.x    | 0.1.x    |
| 0.2.x    | 0.2.x    |

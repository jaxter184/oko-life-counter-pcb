#include <avr/io.h>
#include "numbers.h"
#include "led.h"

// Uncomment to use block number font
//#define USE_BLOCK_NUMBERS

#ifndef USE_BLOCK_NUMBERS
const uint8_t _digit_bmp[12][15] = {
	{0,1,0, 1,0,1, 1,0,1, 1,0,1, 0,1,0}, // 0
	{0,1,0, 0,1,1, 0,1,0, 0,1,0, 1,1,1}, // 1
	{0,1,0, 1,0,1, 0,0,1, 0,1,0, 1,1,1}, // 2
	{1,1,0, 1,0,0, 0,1,0, 1,0,0, 1,1,0}, // 3
	{0,0,1, 1,1,0, 1,1,1, 1,0,0, 0,0,1}, // 4
	{1,1,1, 0,0,1, 1,1,0, 1,0,0, 1,1,0}, // 5
	{0,1,1, 0,0,1, 1,1,0, 1,0,1, 0,1,0}, // 6
	{1,1,1, 1,0,0, 0,1,0, 0,1,0, 0,1,0}, // 7
	{1,1,1, 1,0,1, 0,1,0, 1,0,1, 1,1,1}, // 8
	{0,1,0, 1,0,1, 0,1,1, 1,0,0, 1,1,0}, // 9
	{0,0,0, 1,0,1, 0,1,0, 1,0,1, 0,0,0}, // X (overflow)
	{0,0,0, 0,0,0, 0,1,1, 0,0,0, 0,0,0}, // - (negative sign)
};
#endif
#ifdef USE_BLOCK_NUMBERS
const uint8_t _digit_bmp[12][15] = {
	{1,1,1, 1,0,1, 1,0,1, 1,0,1, 1,1,1}, // 0
	{0,1,0, 0,1,0, 0,1,0, 0,1,0, 0,1,0}, // 1
	{1,1,1, 1,0,0, 1,1,1, 0,0,1, 1,1,1}, // 2
	{1,1,1, 1,0,0, 1,1,1, 1,0,0, 1,1,1}, // 3
	{1,0,1, 1,0,1, 1,1,1, 1,0,0, 0,0,1}, // 4
	{1,1,1, 0,0,1, 1,1,1, 1,0,0, 1,1,1}, // 5
	{1,1,1, 0,0,1, 1,1,1, 1,0,1, 1,1,1}, // 6
	{1,1,1, 1,0,0, 0,0,1, 1,0,0, 0,0,1}, // 7
	{1,1,1, 1,0,1, 1,1,1, 1,0,1, 1,1,1}, // 8
	{1,1,1, 1,0,1, 1,1,1, 1,0,0, 1,1,1}, // 9
	{0,0,0, 1,0,1, 0,1,0, 1,0,1, 0,0,0}, // X (overflow)
	{0,0,0, 0,0,0, 1,1,1, 0,0,0, 0,0,0}, // - (negative sign)
};
#endif

void NUM_set_digit(uint8_t digit, uint8_t offset,
				   uint8_t r, uint8_t g, uint8_t b)
{
	for (int ea_pixel = 0; ea_pixel < 15; ea_pixel++) {
		if (_digit_bmp[digit][ea_pixel]) {
			LED_set_led(ea_pixel + offset, r,g,b); }
		else {
			LED_set_led(ea_pixel + offset, 0,0,0); }
	}
}

void NUM_set_value(int value,
				   uint8_t r, uint8_t g, uint8_t b)
{
	if (value < 0) {
		// Negative
		NUM_set_digit(11, 0, r,g,b);
		// Negative overflow
		if (value < -9) { NUM_set_digit(10,15, r,g,b); }
		else            { NUM_set_digit(-value, 15, r,g,b); }
	}
	else {
		// Positive overflow
		if (value > 99) {
			NUM_set_digit(10, 0, r,g,b);
			NUM_set_digit(10,15, r,g,b);
		}
		// Positive
		else {
			if (value > 9) { NUM_set_digit(value/10,0, r,g,b); }
			else           { NUM_clear(0); }
			NUM_set_digit(value%10,15, r,g,b);
		}
	}
}

void NUM_clear(uint8_t offset) {
	for (int ea_pixel = 0; ea_pixel < 15; ea_pixel++) {
		LED_set_led(ea_pixel + offset, 0,0,0);
	}
}

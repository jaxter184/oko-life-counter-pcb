#include <avr/io.h>
#include "led.h"

#define NUM_LEDS 40
uint8_t _leds[NUM_LEDS][3];

void LED_init() {
    // Data Out:  PA1
    // Clock Out: PA3
    DDRA |= 0b1110;
}

void LED_set_led(uint8_t index, uint8_t r, uint8_t g, uint8_t b) {
	_leds[index][0] = b;
	_leds[index][1] = g;
	_leds[index][2] = r;
}

void LED_send_led(int r, int g, int b){
	// Start frame
	PORTA &= ~0b10;
	for (int i = 0; i < 8 * 4; i++) {
		PORTA &= ~0b1000; PORTA |= 0b1000; // toggle clock
	}
	// Data
	for (int ea_led = 0; ea_led < NUM_LEDS; ea_led++) {
		PORTA |= 0b10;
		for (int ea_bit = 0; ea_bit < 8; ea_bit++) {
			PORTA &= ~0b1000; PORTA |= 0b1000; // toggle clock
		}
		for (int ea_subpix = 0; ea_subpix < 3; ea_subpix++) {
    		for (int ea_bit = 0b10000000; ea_bit > 0; ea_bit>>=1) {
    			PORTA &= ~0b1000;
    			if (ea_bit & _leds[ea_led][ea_subpix]) {
	    			PORTA |= 0b10; }
    			else {
	    			PORTA &=~0b10; }
    			PORTA |= 0b1000;
    		}
		}
	}
	// End frame
	PORTA |= 0b0010;
	for (int i = 0; i < 8 * 4; i++) {
		PORTA &= ~0b1000; PORTA |= 0b1000; // toggle clock
	}
}

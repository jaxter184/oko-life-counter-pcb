#include "led.h"
#include <stdint.h>

#define NUM_PIX 7

uint8_t _pix[NUM_PIX] = {0,0,0,0,0,0};
uint16_t _delay[NUM_PIX] = {200, 840, 280, 100, 640,  500,  70};
uint8_t _target[NUM_PIX] = { 30,  40,  50,  60,  70,  80,  50};

void PIX_render() {
	for (int ea_pix = 0; ea_pix < NUM_PIX; ea_pix++) {
		// Wait for timer to expire
		if (_delay[ea_pix]) { _delay[ea_pix]--; }
		else {
			// Slope up and down
			if (_pix[ea_pix] < _target[ea_pix])      { _pix[ea_pix]++; }
			else if (_pix[ea_pix] > _target[ea_pix]) { _pix[ea_pix]--; }
			else { // if _pix == _target
				if (_target[ea_pix]) { // Reaches peak
					// Flip slope downward
					_target[ea_pix] = 0;
				}
				else { // End of slope cycle
					// pseudorandomization magic
					_delay[ea_pix] =  ((_pix[(ea_pix+1)%6] +
					                    _delay[(ea_pix+3)%6] +
					                    _pix[(ea_pix+2)%6] +
										_delay[(ea_pix+6)%6] +
										_pix[(ea_pix+4)%6]) ^ 0xff) + 50;
					_target[ea_pix] = ((_pix[(ea_pix+3)%6] +
					                    _target[(ea_pix+4)%6]) % 50) + 30;
				}
			}
		}
		LED_set_led(33 + ea_pix, 0,_pix[ea_pix],_pix[ea_pix]);
	}
}

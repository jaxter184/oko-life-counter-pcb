#include "life-counter.h"
#include "led.h"
#include "numbers.h"
#include "switch.h"

uint8_t _show_idx = 0; // Make sure these are big enough
uint8_t _head_idx = 0;
#define HIST_SIZE 16 // Must be a power of 2
int _life[HIST_SIZE] = {20};
uint8_t _hist_len = 0;
uint8_t _saved = 1;
int _diff = 0;

#define COUNTDOWN_TIME 0x1f
uint16_t countdown = 0;

uint8_t _sw0 = 0;
uint8_t _sw1 = 0;
uint8_t _sw2 = 0;

void _increment() {
	countdown = COUNTDOWN_TIME;
	if (_saved) {
		_saved = 0;
	}
	if (_sw0) { _diff++;  }
	if (_sw1) { _diff--;  }
	if (_sw2) { _diff-=6; }
}

void MODELC_render() {
//## Static colors
	LED_set_led(30,  0x3,   0,   0); // Eldraine logo
	LED_set_led(31,    0,   0, 0x3); // Island mana
	LED_set_led(32,    0, 0x3,   0); // Forest mana

//## Read inputs
	_sw0 = SWITCH_get(0);
	_sw1 = SWITCH_get(1);
	_sw2 = SWITCH_get(2);

//### Undo history
	if (SWITCH_state(3)) {
		if (!_saved && (_sw0 || _sw1 || _sw2) ) {
			// Don't do history shenanigans when in diff mode
			_increment();
		}
		else {
			if (_sw0 && (_show_idx < _hist_len)) { _show_idx++; }
			if (_sw1 && _show_idx)               { _show_idx--; }
			if (_sw2)                            { _show_idx=0; }
		}
	}

//### Standard incrementation
	else {
		if (_sw0 || _sw1 || _sw2) {
			_increment();
		}
		if (countdown) {
			// Redo history should be erased unless diff is 0
			if (countdown == 1) {
				if (_diff != 0) {
					// Erase history
					if (_show_idx) {
						_head_idx -= _show_idx;
						_hist_len -= _show_idx;
						_show_idx = 0;
					}
					if (_hist_len < HIST_SIZE-1) { _hist_len++; }
					// Write new life total
					int prev_life = _life[_head_idx];
					_head_idx = (_head_idx+1)&(HIST_SIZE-1);
					_life[_head_idx] = prev_life + _diff;
					_diff = 0;
				}
				// Display life total
				_saved = 1;
			}
			countdown--;
		}
	}

//## Render digits
	if (_saved) {
		if ( SWITCH_state(3) && ( SWITCH_state(0) ||
		                          SWITCH_state(1) ||
		                          SWITCH_state(2) ) )
		{ NUM_set_value(_show_idx, 0,0,3); }
		else { NUM_set_value(_life[_head_idx - _show_idx], 1,1,1); }
	}
	else {
		if (_diff < 0) {
			if (_diff < -9) { NUM_set_value(-_diff, 3,0,0); }
			else { NUM_set_value(_diff, 3,0,0); }
		}
		else { NUM_set_value(_diff, 0,3,0); }
	}
}

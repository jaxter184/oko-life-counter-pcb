//#define F_CPU 20000000UL
//#include <avr/io.h>
#include "led.h"
#include "switch.h"
#include "life-counter.h"
#include "pixies.h"

enum MODE {
	NONE,
	LIFE_COUNT,
	OKO,
} mode = 1;

int main(void)
{
	LED_init();
	SWITCH_init();
//	DDRB	&=~0b11;
//	PORTB	|= 0b01;
	while(1){
		PIX_render();
		SWITCH_update();
		switch (mode) {
			case LIFE_COUNT:
				MODELC_render();
				break;
			case OKO:
				break;
			default:
				break;
		}

		LED_send_led();
	}
}

#include <avr/io.h>
#include "switch.h"

uint8_t _switches[7] = {0,0,0,0,0,0,0};

void SWITCH_init() {
	// Front switches: (top) PA7, PB0, PB1 (bottom)
	// Back switches: (top) PA2, PA4, PA5, PA6 (bottom)
	DDRA  &=~0b11110100; DDRB  &=~0b11; // Configure as inputs
	PORTA |= 0b11110100; PORTB |= 0b11; // Internal pullups
}

uint8_t SWITCH_get(uint8_t index) {
	uint8_t output = 0;
	if (_switches[index]) {
		output = 1;
		_switches[index]--;
	}
	return output;
}

uint8_t SWITCH_state(uint8_t index) {
	switch (index) {
		case 0:
			return !((PINA >> 7) & 1);
		case 1:
			return !((PINB >> 0) & 1);
		case 2:
			return !((PINB >> 1) & 1);
		case 3:
			return !((PINA >> 2) & 1);
		case 4:
			return !((PINA >> 4) & 1);
		case 5:
			return !((PINA >> 5) & 1);
		case 6:
			return !((PINA >> 6) & 1);
		default:
			return 0;
	}
}

uint8_t _sw_raw[7]   = {1,1,1,1,1,1,1};
uint8_t _p_sw_raw[7] = {1,1,1,1,1,1,1};

void SWITCH_update() {
	uint8_t register_a_raw = PINA;
	uint8_t register_b_raw = PINB;
	_sw_raw[0] = (register_a_raw >> 7) & 1;
	_sw_raw[1] = (register_b_raw >> 0) & 1;
	_sw_raw[2] = (register_b_raw >> 1) & 1;
	_sw_raw[3] = (register_a_raw >> 2) & 1;
	_sw_raw[4] = (register_a_raw >> 4) & 1;
	_sw_raw[5] = (register_a_raw >> 5) & 1;
	_sw_raw[6] = (register_a_raw >> 6) & 1;
	for (int ea_switch = 0; ea_switch < 7; ea_switch++) {
		if (!_sw_raw[ea_switch] && _p_sw_raw[ea_switch]) {
			_switches[ea_switch]++;
		}
		_p_sw_raw[ea_switch] = _sw_raw[ea_switch]; // TODO: ping pong buffer
	}
}

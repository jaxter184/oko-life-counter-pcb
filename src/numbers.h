#ifndef NUM_H
#define NUM_H
#include <stdint.h>

void NUM_set_digit(uint8_t digit, uint8_t offset,
				   uint8_t r, uint8_t g, uint8_t b);
void NUM_set_value(int value,
				   uint8_t r, uint8_t g, uint8_t b);
void NUM_clear(uint8_t offset);

#endif

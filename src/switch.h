#ifndef SWITCH_H
#define SWITCH_H
#include <stdint.h>

void SWITCH_init();
uint8_t SWITCH_get(uint8_t index);
uint8_t SWITCH_state(uint8_t index);
void SWITCH_update();

#endif

#!/usr/bin/env python3
import re

files = ["okofsilk.kicad_mod", "okofmask.kicad_mod", "okofcu.kicad_mod"]
text = []

if __name__ == '__main__':
	## read each file
	for ea_file in files:
		with open(ea_file, "r") as f:
#			removed_registers = re.sub('\n.*31\.496000.*\n  \)', '', f.read())
			removed_registers = re.sub('\n.*44\.023843.*\n  \)', '', f.read())
			removed_registers = re.sub('\n.*40\.402835.*\n  \)', '', removed_registers)
			removed_registers = re.sub('\n.*42\.0.*.*\n  \)', '', removed_registers)
			head_and_tailed = '\n'.join(removed_registers.split('\n')[8:-2])
			text.append(head_and_tailed)
	## add header and footer
	add_header_and_footer = """(module LOGO (layer F.Cu)
  (at 0 0)
 (fp_text reference "G***" (at 0 0) (layer F.SilkS) hide
  (effects (font (thickness 0.3)))
  )
  (fp_text value "LOGO" (at 0.75 0) (layer F.SilkS) hide
  (effects (font (thickness 0.3)))
  )
""" + '\n'.join(text) + '\n)'
	output = re.sub('Eco1\.User', 'F.Cu', add_header_and_footer)

	## save combined file
	with open("oko.kicad_mod", "w") as f:
			f.write(output)
